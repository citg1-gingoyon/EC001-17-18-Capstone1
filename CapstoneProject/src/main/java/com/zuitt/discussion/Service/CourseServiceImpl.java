package com.zuitt.discussion.Service;

import com.zuitt.discussion.Repositories.CourseRepository;
import com.zuitt.discussion.Repositories.UserRepository;
import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createCourse(String stringToken, Course course) {
        //Retrieve the "User" object using the extracted username from the JWT Token
        User enrolled = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(enrolled);
        courseRepository.save(newCourse);

    }

    //    Get All Posts
    public Iterable<Course> getPosts(){
        return courseRepository.findAll();
    }

    //    Delete post
    public ResponseEntity deletePost(String stringToken, Long id){
       /* postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);*/
        Course postForDeleting = courseRepository.findById(id).get();

        String postAuthorName = postForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(postAuthorName)){
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Post Deleted successfully", HttpStatus.OK);

        }
        else{
            return new ResponseEntity<>("You are not authorize to delete this post",HttpStatus.UNAUTHORIZED);

        }
    }

    //    Update post
    public ResponseEntity updatePost(Long id,String stringToken, Course course){
/*//        Find the post to update
        Post postForUpdate = postRepository.findById(id).get();

//        Updating the title and content
        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());

//        Saving and updating a post
        postRepository.save(postForUpdate);

        return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);*/

        Course courseForUpdating = courseRepository.findById(id).get();
//  Fet the author of the specific post
        String postAuthor = courseForUpdating.getUser().getUsername();
//  Get the username from the stringToken to compare it with the username of the current post being edited
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);
//  check if the username of the authenticated user matches the post's user
        if(authenticatedUserName.equals(postAuthor)){
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseRepository.save(courseForUpdating);

            return new ResponseEntity<>("Post updated successfully",HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorize to edit this post",HttpStatus.UNAUTHORIZED);
        }
    }

    /*//  Get users post
    public  Iterable<Course> getMyPosts(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourse();
    }*/

}
